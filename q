package common.constants;

import java.util.Arrays;

/**
 * ボタン判別用の値ENUM
 *
 * @author oracle
 *
 */
public enum CarouselButtonVal {

    SOUNAN("sounan", "遭難"), YUKIKAKI("yukikaki", "雪かき"), UCHU("uchu", "宇宙"), MUEN("muen", "無縁"), UWAKI("uwaki", "浮気"),
    TRAINING("training","トレーニング"), ODEKAKE("odekake","おでかけ"), AMBASSADOR("ambassador", "アンバサダー");

    private final String buttonValue;
    private final String hokenName;

    private CarouselButtonVal(final String buttonValue, final String hokenName) {
        this.buttonValue = buttonValue;
        this.hokenName = hokenName;
    }

    /**
     * buttonValue(ボタン名)からhokenName(保険名)を取得
     *
     * @param buttonValue ボタン名
     * @return 保険名
     */
    public static String getHokenNameByButtonVal(final String buttonValue) {
        return Arrays.stream(CarouselButtonVal.values())
                .filter(data -> data.getButtonValue().equals(buttonValue))
                .map(data -> data.getHokenName())
                .findFirst()
                .orElse(null);
    }

    /**
     * ボタン名を取得
     *
     * @return ボタン名
     */
    public String getButtonValue() {
        return this.buttonValue;
    }

    /**
     * 保険名を取得
     *
     * @return 保険名
     */
    public String getHokenName() {
        return this.hokenName;
    }
}
