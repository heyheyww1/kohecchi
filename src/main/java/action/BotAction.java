package action;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.template.ButtonsTemplate;
import com.linecorp.bot.model.message.template.CarouselTemplate;

import builder.CarouselBuilder;
import builder.DateButtonBuilder;
import common.constants.CarouselButtonVal;
import common.dto.NewContractDTO;
import common.dto.NewOdekakeDTO;
import common.dto.StoryDTO;
import common.dto.TimesDTO;
import common.info.AppScopeUserInfo;
import common.info.ReqScopeUserBotInfo;
import rest.dto.RestKeiyaku;
import service.EntityActionService;

/**
 * LINEBOTのアクションを行うクラス
 *
 * @author oracle
 *
 */
@Dependent
public class BotAction {

	private static final Logger LOGGER = LoggerFactory.getLogger((new Object() {
	}.getClass().getEnclosingClass()));

	@Inject
	private AppScopeUserInfo appScopeUserInfo;

	@Inject
	private ReqScopeUserBotInfo reqScopeUserBotInfo;

	@Inject
	private EntityActionService entityActionService;

	private StoryDTO storyDTO;
	private TimesDTO timesDTO;
	private String userId;
	private List<Message> botMessageList;

	/**
	 * ストーリーカテゴリ別にストーリーを分岐させる
	 *
	 * @param userId LINEユーザID
	 */
	public void execute(final String userId) {

		this.userId = userId;
		// LINEユーザ用の情報保持Mapを取得
		storyDTO = appScopeUserInfo.getStoryMap().get(userId);
		timesDTO = appScopeUserInfo.getTimesMap().get(userId);

		final String storyCategory = storyDTO.getStoryCategory();
		final String timesCategory = timesDTO.getTimesCategory();
		final String times = timesDTO.getTimes();

		LOGGER.info("========story_category==========" + storyCategory);
		LOGGER.info("========story==========" + storyDTO.getStory());
		LOGGER.info("========timesCategory==========" + timesDTO.getTimesCategory());
		LOGGER.info("========times==========" + timesDTO.getTimes());

		botMessageList = reqScopeUserBotInfo.getBotMessageList();
		//LINEユーザーの送信したメッセージを取得
		final String userMessage = reqScopeUserBotInfo.getUserMessage();

		if ("キャンセル".equals(userMessage)) {
			// ストーリーを初期化
			storyDTO.setStory("none");
			storyDTO.setStoryCategory("none");
			botMessageList.add(new TextMessage("キャンセルしました"));
		}

		// BOTメッセージを設定
		switch (storyCategory) {
		case "none":
			/* ストーリーが始まっていない場合:ストーリー開始 */

			System.out.println("userMessage:" + userMessage);

			// BOTメッセージを設定
			switch (userMessage) {
			case "新規契約":
				/* 新規契約が送信された場合 */

				// ストーリーカテゴリを新規契約にする
				storyDTO.setStoryCategory(userMessage);

				// 新規契約用DTOをインスタンス化
				appScopeUserInfo.getNewContractMap().put(userId, new NewContractDTO());

				// BOTメッセージを設定
				botMessageList.add(new TextMessage("新規契約を開始します！"));
				botMessageList.add(new TextMessage("契約したい保険を選んでください"));
				// 保険情報カルーセルを生成
				final CarouselTemplate carousel = CarouselBuilder.create();
				botMessageList.add(new TemplateMessage("カルーセル", carousel));

				// 次回のストーリーを設定
				storyDTO.setStory("1");
				break;

			case "こへっち":
				/* スポーツが送信された場合 */
				LOGGER.info("kohecchi");

				// ストーリーカテゴリをこへっちモードにする
				storyDTO.setStoryCategory(userMessage);

				// BOTメッセージを設定
				botMessageList.add(new TextMessage("今日は何をするじょ？"));

				// カルーセルを生成
				final CarouselTemplate kohecchiCarousel = CarouselBuilder.kohecchiCreate();
				botMessageList.add(new TemplateMessage("カルーセル", kohecchiCarousel));

				// 次回のストーリーを設定
				storyDTO.setStory("1");
				break;
			}
			break;

		case "新規契約":
			/* 新規契約ストーリーの場合 */
			proceedNewContruct();
			break;

		case "こへっち":
			/* こへっちストーリーの場合 */
			proceedNewTraining();
			break;
		}
	}

	/**
	 * trainingストーリーを進める
	 *
	 * @param userId LINEユーザID
	 */
	private void proceedNewTraining() {
		// 現在のストーリーを取得
		final String story = storyDTO.getStory();

		// 新規契約用DTOを取得
		final NewContractDTO newContractDTO = appScopeUserInfo.getNewContractMap().get(userId);
		
		// ユーザメッセージを取得
		final String userMessage = reqScopeUserBotInfo.getUserMessage();

		switch (story) {
		case "1":
			// LINEユーザーの送信したメッセージを取得
			final String worktype1 = reqScopeUserBotInfo.getUserMessage();
			LOGGER.info("worktype1 = " + worktype1);
			// ボタン値末尾の数字を削除
			final String buttonVal = worktype1.substring(0, worktype1.length() - 1);
			//final String hokenName = CarouselButtonVal.getHokenNameByButtonVal(buttonVal);

			switch (worktype1) {
			case "training1":
				botMessageList.add(new TextMessage("腹筋ですね"));
				botMessageList.add(new TextMessage("やりすぎて腰を痛めるなじょ"));
				timesDTO.setTimesCategory("腹筋");
				break;
			case "training2":
				botMessageList.add(new TextMessage("背筋ですね"));
				botMessageList.add(new TextMessage("反りすぎて腰を痛めるなじょ"));
				timesDTO.setTimesCategory("背筋");
				break;
			case "training3":
				botMessageList.add(new TextMessage("大車輪ですね"));
				//botMessageList.add(new TextMessage("うんこするじょ"));
				timesDTO.setTimesCategory("大車輪");
				break;
			}
			botMessageList.add(new TextMessage("回数を教えるじょ"));

			storyDTO.setStory("2");
			break;

		case "2":
			/* ストーリー2:回数を入力 */

			// トレーニングカテゴリを保存
			timesDTO.setTimes(userMessage);
			if (Integer.parseInt(userMessage) > 1000) {
				botMessageList.add(new TextMessage("すごいじょ！レベルアップだじょ！"));
			}

			// BOTメッセージを設定
			botMessageList.add(new TextMessage(userMessage + "回ですね！"));
			botMessageList.add(new TextMessage("保険の開始日を入力してください"));
			final ButtonsTemplate button2 = DateButtonBuilder.create("保険の開始日", "startdate");
			botMessageList.add(new TemplateMessage("ボタン", button2));

			// 次回のストーリーを設定
			storyDTO.setStory("3");

			break;

		case "3":
			/* ストーリー3:終了日入力 */

			// LINEユーザーの送信したメッセージを取得
			final String startdate3 = reqScopeUserBotInfo.getUserMessage();
			if (startdate3.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")) {

				// 開始日を保存
				newContractDTO.setStartdate(startdate3);

				// BOTメッセージを設定
				botMessageList.add(new TextMessage("開始日は" + newContractDTO.getStartdate() + "ですね！"));
				botMessageList.add(new TextMessage("保険の終了日を入力してください"));
				final ButtonsTemplate button3 = DateButtonBuilder.create("保険の終了日", "enddate");
				botMessageList.add(new TemplateMessage("ボタン", button3));

				// 次回のストーリーを設定
				storyDTO.setStory("4");
			} else {
				botMessageList.add(new TextMessage("日付選択から開始日を選択してください"));
			}
			break;

		case "4":
			/* ストーリー4:契約情報登録 */

			// LINEユーザーの送信したメッセージを取得
			final String enddate4 = reqScopeUserBotInfo.getUserMessage();
			if (enddate4.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")) {
				final int startdateInt = Integer
						.parseInt(newContractDTO.getStartdate().replaceAll("-", ""));
				final int enddateInt = Integer.parseInt(enddate4.replaceAll("-", ""));
				if (startdateInt < enddateInt) {

					// 終了日を保存
					newContractDTO.setEnddate(enddate4);

					/* mockのREST APIを実行する */
					final String kokyakuname4 = newContractDTO.getKokyakuname();

					// 契約情報を登録する
					final String worktype4 = newContractDTO.getWorktype();
					final String hokenmei = worktype4.substring(0, worktype4.length() - 1);
					final String startdate = newContractDTO.getStartdate();
					final String enddate = newContractDTO.getEnddate();
					final RestKeiyaku restKeiyaku = new RestKeiyaku(null, userId, kokyakuname4,
							11111L, hokenmei, startdate, enddate);
					final RestKeiyaku result = entityActionService.createKeiyaku(restKeiyaku);

					// BOTメッセージを設定
					botMessageList.add(new TextMessage("終了日は" + newContractDTO.getEnddate() + "ですね！"));
					botMessageList.add(new TextMessage(
							"契約番号" + result.getKeiyakuId() + "で契約が完了しました！"));
					botMessageList.add(new TextMessage("ご契約頂いた内容は「契約情報」から確認できます。"));

					// ストーリーを初期化
					storyDTO.setStory("none");
					storyDTO.setStoryCategory("none");

				} else {
					botMessageList.add(new TextMessage("終了日は開始日より後の日付を選択してください"));
				}
			} else {
				botMessageList.add(new TextMessage("日付選択から終了日を選択してください"));
			}
			break;

		case "5":
			// ユーザーメッセージを保存
			// BOTメッセージを設定
			// 次回のストーリーを設定
			storyDTO.setStory("none");
			storyDTO.setStoryCategory("none");
			break;
		}

	}

	/**
	 * 新規契約ストーリーを進める
	 *
	 * @param userId LINEユーザID
	 */
	private void proceedNewContruct() {
		// 現在のストーリーを取得
		final String story = storyDTO.getStory();

		// 新規契約用DTOを取得
		final NewContractDTO newContractDTO = appScopeUserInfo.getNewContractMap().get(userId);

		switch (story) {
		case "1":
			/* ストーリー1:氏名入力 */

			// LINEユーザーの送信したメッセージを取得
			final String worktype1 = reqScopeUserBotInfo.getUserMessage();
			// ボタン値末尾の数字を削除
			final String buttonVal = worktype1.substring(0, worktype1.length() - 1);
			final String hokenName = CarouselButtonVal.getHokenNameByButtonVal(buttonVal);
			if (hokenName != null) {

				// 契約内容を保存
				newContractDTO.setWorktype(worktype1);

				// BOTメッセージを設定
				botMessageList.add(new TextMessage(hokenName + "保険ですね！"));
				botMessageList.add(new TextMessage("契約者の氏名をスペースを入れずに入力して下さい。"));

				// 次回のストーリーを設定
				storyDTO.setStory("2");
			} else {
				botMessageList.add(new TextMessage("ボタンから契約する保険を選択してください"));
			}
			break;

		case "2":
			/* ストーリー2:開始日入力 */

			// LINEユーザーの送信したメッセージを取得
			final String kokyakuname2 = reqScopeUserBotInfo.getUserMessage();
			if (kokyakuname2.length() <= 10) {

				// 顧客名を保存
				newContractDTO.setKokyakuname(kokyakuname2);

				// BOTメッセージを設定
				botMessageList.add(new TextMessage(newContractDTO.getKokyakuname() + "さんですね！"));
				botMessageList.add(new TextMessage("保険の開始日を入力してください"));
				final ButtonsTemplate button2 = DateButtonBuilder.create("保険の開始日", "startdate");
				botMessageList.add(new TemplateMessage("ボタン", button2));

				// 次回のストーリーを設定
				storyDTO.setStory("3");

			} else {
				botMessageList.add(new TextMessage("氏名は10文字以下で入力してください"));
			}
			break;

		case "3":
			/* ストーリー3:終了日入力 */

			// LINEユーザーの送信したメッセージを取得
			final String startdate3 = reqScopeUserBotInfo.getUserMessage();
			if (startdate3.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")) {

				// 開始日を保存
				newContractDTO.setStartdate(startdate3);

				// BOTメッセージを設定
				botMessageList.add(new TextMessage("開始日は" + newContractDTO.getStartdate() + "ですね！"));
				botMessageList.add(new TextMessage("保険の終了日を入力してください"));
				final ButtonsTemplate button3 = DateButtonBuilder.create("保険の終了日", "enddate");
				botMessageList.add(new TemplateMessage("ボタン", button3));

				// 次回のストーリーを設定
				storyDTO.setStory("4");
			} else {
				botMessageList.add(new TextMessage("日付選択から開始日を選択してください"));
			}
			break;

		case "4":
			/* ストーリー4:契約情報登録 */

			// LINEユーザーの送信したメッセージを取得
			final String enddate4 = reqScopeUserBotInfo.getUserMessage();
			if (enddate4.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")) {
				final int startdateInt = Integer
						.parseInt(newContractDTO.getStartdate().replaceAll("-", ""));
				final int enddateInt = Integer.parseInt(enddate4.replaceAll("-", ""));
				if (startdateInt < enddateInt) {

					// 終了日を保存
					newContractDTO.setEnddate(enddate4);

					/* mockのREST APIを実行する */
					final String kokyakuname4 = newContractDTO.getKokyakuname();

					// 契約情報を登録する
					final String worktype4 = newContractDTO.getWorktype();
					final String hokenmei = worktype4.substring(0, worktype4.length() - 1);
					final String startdate = newContractDTO.getStartdate();
					final String enddate = newContractDTO.getEnddate();
					final RestKeiyaku restKeiyaku = new RestKeiyaku(null, userId, kokyakuname4,
							11111L, hokenmei, startdate, enddate);
					final RestKeiyaku result = entityActionService.createKeiyaku(restKeiyaku);

					// BOTメッセージを設定
					botMessageList.add(new TextMessage("終了日は" + newContractDTO.getEnddate() + "ですね！"));
					botMessageList.add(new TextMessage(
							"契約番号" + result.getKeiyakuId() + "で契約が完了しました！"));
					botMessageList.add(new TextMessage("ご契約頂いた内容は「契約情報」から確認できます。"));

					// ストーリーを初期化
					storyDTO.setStory("none");
					storyDTO.setStoryCategory("none");

				} else {
					botMessageList.add(new TextMessage("終了日は開始日より後の日付を選択してください"));
				}
			} else {
				botMessageList.add(new TextMessage("日付選択から終了日を選択してください"));
			}
			break;

		case "5":
			// ユーザーメッセージを保存
			// BOTメッセージを設定
			// 次回のストーリーを設定
			storyDTO.setStory("none");
			storyDTO.setStoryCategory("none");
			break;
		}
	}

	/**
	 * 新規契約ストーリーを進める
	 *
	 * @param userId LINEユーザID
	 */
	private void executeKohecchi() {

		this.userId = userId;
		// LINEユーザ用の情報保持Mapを取得
		storyDTO = appScopeUserInfo.getStoryMap().get(userId);

		final String storyCategory = storyDTO.getStoryCategory();

		LOGGER.info("========story_category==========" + storyCategory);
		LOGGER.info("========story==========" + storyDTO.getStory());

		botMessageList = reqScopeUserBotInfo.getBotMessageList();

		// BOTメッセージを設定
		switch (storyCategory) {
		case "none":
			/* ストーリーが始まっていない場合:ストーリー開始 */

			// LINEユーザーの送信したメッセージを取得
			final String worktype1 = reqScopeUserBotInfo.getUserMessage();
			final String buttonVal = worktype1.substring(0, worktype1.length() - 1);
			final String modeName = CarouselButtonVal.getHokenNameByButtonVal(buttonVal);

			// BOTメッセージを設定
			switch (modeName) {
			case "トレーニング":
				/* 新規契約が送信された場合 */

				// ストーリーカテゴリをトレーニングにする
				storyDTO.setStoryCategory(modeName);

				// 新規契約用DTOをインスタンス化
				appScopeUserInfo.getNewContractMap().put(userId, new NewContractDTO());

				// BOTメッセージを設定
				botMessageList.add(new TextMessage("トレーニングモードを開始するじょ"));

				// 次回のストーリーを設定
				storyDTO.setStory("1");
				break;

			case "おでかけ":
				/* 新規契約が送信された場合 */

				// ストーリーカテゴリを新規契約にする
				storyDTO.setStoryCategory(modeName);

				// 新規契約用DTOをインスタンス化
				appScopeUserInfo.getNewContractMap().put(userId, new NewContractDTO());

				// BOTメッセージを設定
				botMessageList.add(new TextMessage("おでかけモードを開始するじょ"));

				// 次回のストーリーを設定
				storyDTO.setStory("1");
				break;
			}
			break;

		case "トレーニング":
			/* 新規契約ストーリーの場合 */

			proceedNewContruct();
			break;

		case "おでかけ":

			proceedNewOdekake();
			break;
		}

	}

	/**
	 * 新規契約ストーリーを進める
	 *
	 * @param userId LINEユーザID
	 */
	private void proceedNewOdekake() {
		// 現在のストーリーを取得
		final String story = storyDTO.getStory();

		// 新規契約用DTOを取得
		final NewOdekakeDTO newOdekakeDTO = appScopeUserInfo.getNewOdekakeMap().get(userId);

		switch (story) {
		case "1":
			/* ストーリー1:氏名入力 */

			// LINEユーザーの送信したメッセージを取得
			final String worktype1 = reqScopeUserBotInfo.getUserMessage();
			// ボタン値末尾の数字を削除
			final String buttonVal = worktype1.substring(0, worktype1.length() - 1);
			final String modeName = CarouselButtonVal.getHokenNameByButtonVal(buttonVal);
			if (modeName != null) {

				// 契約内容を保存
				newOdekakeDTO.setWorktype(worktype1);

				// BOTメッセージを設定
				botMessageList.add(new TextMessage(modeName + "をやるんだじょ"));
				botMessageList.add(new TextMessage("契約者の氏名をスペースを入れずに入力して下さい。"));

				// 次回のストーリーを設定
				storyDTO.setStory("2");
			} else {
				botMessageList.add(new TextMessage("ボタンから契約する保険を選択してください"));
			}
			break;

		case "2":
			/* ストーリー2:開始日入力 */

			// LINEユーザーの送信したメッセージを取得
			final String kokyakuname2 = reqScopeUserBotInfo.getUserMessage();
			if (kokyakuname2.length() <= 10) {

				// 顧客名を保存
				newOdekakeDTO.setKokyakuname(kokyakuname2);

				// BOTメッセージを設定
				botMessageList.add(new TextMessage(newOdekakeDTO.getKokyakuname() + "さんですね！"));
				botMessageList.add(new TextMessage("保険の開始日を入力してください"));
				final ButtonsTemplate button2 = DateButtonBuilder.create("保険の開始日", "startdate");
				botMessageList.add(new TemplateMessage("ボタン", button2));

				// 次回のストーリーを設定
				storyDTO.setStory("3");

			} else {
				botMessageList.add(new TextMessage("氏名は10文字以下で入力してください"));
			}
			break;

		case "3":
			/* ストーリー3:終了日入力 */

			// LINEユーザーの送信したメッセージを取得
			final String startdate3 = reqScopeUserBotInfo.getUserMessage();
			if (startdate3.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")) {

				// 開始日を保存
				newOdekakeDTO.setStartdate(startdate3);

				// BOTメッセージを設定
				botMessageList.add(new TextMessage("開始日は" + newOdekakeDTO.getStartdate() + "ですね！"));
				botMessageList.add(new TextMessage("保険の終了日を入力してください"));
				final ButtonsTemplate button3 = DateButtonBuilder.create("保険の終了日", "enddate");
				botMessageList.add(new TemplateMessage("ボタン", button3));

				// 次回のストーリーを設定
				storyDTO.setStory("4");
			} else {
				botMessageList.add(new TextMessage("日付選択から開始日を選択してください"));
			}
			break;

		case "4":
			/* ストーリー4:契約情報登録 */

			// LINEユーザーの送信したメッセージを取得
			final String enddate4 = reqScopeUserBotInfo.getUserMessage();
			if (enddate4.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")) {
				final int startdateInt = Integer
						.parseInt(newOdekakeDTO.getStartdate().replaceAll("-", ""));
				final int enddateInt = Integer.parseInt(enddate4.replaceAll("-", ""));
				if (startdateInt < enddateInt) {

					// 終了日を保存
					newOdekakeDTO.setEnddate(enddate4);

					/* mockのREST APIを実行する */
					final String kokyakuname4 = newOdekakeDTO.getKokyakuname();

					// 契約情報を登録する
					final String worktype4 = newOdekakeDTO.getWorktype();
					final String hokenmei = worktype4.substring(0, worktype4.length() - 1);
					final String startdate = newOdekakeDTO.getStartdate();
					final String enddate = newOdekakeDTO.getEnddate();
					final RestKeiyaku restKeiyaku = new RestKeiyaku(null, userId, kokyakuname4,
							11111L, hokenmei, startdate, enddate);
					final RestKeiyaku result = entityActionService.createKeiyaku(restKeiyaku);

					// BOTメッセージを設定
					botMessageList.add(new TextMessage("終了日は" + newOdekakeDTO.getEnddate() + "ですね！"));
					botMessageList.add(new TextMessage(
							"契約番号" + result.getKeiyakuId() + "で契約が完了しました！"));
					botMessageList.add(new TextMessage("ご契約頂いた内容は「契約情報」から確認できます。"));

					// ストーリーを初期化
					storyDTO.setStory("none");
					storyDTO.setStoryCategory("none");

				} else {
					botMessageList.add(new TextMessage("終了日は開始日より後の日付を選択してください"));
				}
			} else {
				botMessageList.add(new TextMessage("日付選択から終了日を選択してください"));
			}
			break;

		case "5":
			// ユーザーメッセージを保存
			// BOTメッセージを設定
			// 次回のストーリーを設定
			storyDTO.setStory("none");
			storyDTO.setStoryCategory("none");
			break;
		}
	}
}
