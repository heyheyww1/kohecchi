package builder;

import java.util.Arrays;
import java.util.List;

import com.linecorp.bot.model.action.Action;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.template.CarouselColumn;
import com.linecorp.bot.model.message.template.CarouselTemplate;

import common.constants.CarouselButtonDispName;
import common.constants.CarouselButtonVal;

/**
 * カルーセルを生成するビルダークラス
 *
 */
public class CarouselBuilder {

	/**
	 * カルーセルを生成
	 *
	 * @return カルーセルテンプレート
	 */
	public static CarouselTemplate create(){

			final String img1 = "https://d2dcan0armyq93.cloudfront.net/photo/odai/600/3b18ba340196b0a1ba064e9b6aa2cc71_600.jpg";
			final String title1 = "遭難保険";
			final String text1 = "遭難してしまった時にご契約頂ければ、即時に救助へ向かいます！";
			final Action Act11 = new PostbackAction(CarouselButtonDispName.KEIYAKU.getDispName(), CarouselButtonVal.SOUNAN.getButtonValue() + 1);
			final Action Act12 = new PostbackAction(CarouselButtonDispName.KAIYAKU.getDispName(), CarouselButtonVal.SOUNAN.getButtonValue() + 2);
			final Action Act13 = new PostbackAction(CarouselButtonDispName.KEIYAKU_HENKO.getDispName(), CarouselButtonVal.SOUNAN.getButtonValue() + 3);

			final String img2 = "https://i2.wp.com/gogon.net/sozaiya_gogon/wp-content/uploads/2010/06/i12-72a-01.png";
			final String title2 = "雪かき保険";
			final String text2 = "雪かき中の怪我による費用を補償します！";
			final Action Act21 = new PostbackAction(CarouselButtonDispName.KEIYAKU.getDispName(), CarouselButtonVal.YUKIKAKI.getButtonValue() + 1);
			final Action Act22 = new PostbackAction(CarouselButtonDispName.KAIYAKU.getDispName(), CarouselButtonVal.YUKIKAKI.getButtonValue() + 2);
			final Action Act23 = new PostbackAction(CarouselButtonDispName.KEIYAKU_HENKO.getDispName(), CarouselButtonVal.YUKIKAKI.getButtonValue() + 3);

			final String img3 = "https://iwiz-blog-cms.c.yimg.jp/c/blog-cms/series/feature/sc_top.jpg";
			final String title3 = "宇宙旅行保険";
			final String text3 = "宇宙空間へ旅行する際に、降りかかった不幸を補償します！";
			final Action Act31 = new PostbackAction(CarouselButtonDispName.KEIYAKU.getDispName(), CarouselButtonVal.UCHU.getButtonValue() + 1);
			final Action Act32 = new PostbackAction(CarouselButtonDispName.KAIYAKU.getDispName(), CarouselButtonVal.UCHU.getButtonValue() + 2);
			final Action Act33 = new PostbackAction(CarouselButtonDispName.KEIYAKU_HENKO.getDispName(), CarouselButtonVal.UCHU.getButtonValue() + 3);

			final String img4 = "https://person-illustration.com/material/154-person-illustration.jpg";
			final String title4 = "無縁社会保険";
			final String text4 = "無縁社会となった貴方を補償します！";
			final Action Act41 = new PostbackAction(CarouselButtonDispName.KEIYAKU.getDispName(), CarouselButtonVal.MUEN.getButtonValue() + 1);
			final Action Act42 = new PostbackAction(CarouselButtonDispName.KAIYAKU.getDispName(), CarouselButtonVal.MUEN.getButtonValue() + 2);
			final Action Act43 = new PostbackAction(CarouselButtonDispName.KEIYAKU_HENKO.getDispName(), CarouselButtonVal.MUEN.getButtonValue() + 3);

			final String img5 = "https://season-calendar.com/wp-content/uploads/2017/09/hurin1.png";
			final String title5 = "浮気&離婚保険";
			final String text5 = "浮気や、離婚等、人生の岐路に立たされた時の費用を補償します！";
			final Action Act51 = new PostbackAction(CarouselButtonDispName.KEIYAKU.getDispName(), CarouselButtonVal.UWAKI.getButtonValue() + 1);
			final Action Act52 = new PostbackAction(CarouselButtonDispName.KAIYAKU.getDispName(), CarouselButtonVal.UWAKI.getButtonValue() + 2);
			final Action Act53 = new PostbackAction(CarouselButtonDispName.KEIYAKU_HENKO.getDispName(), CarouselButtonVal.UWAKI.getButtonValue() + 3);

			final List<Action> actions1 = Arrays.asList(Act11, Act12, Act13);
			final CarouselColumn crousel1 = new CarouselColumn(img1, title1, text1, actions1);

			final List<Action> actions2 = Arrays.asList(Act21, Act22, Act23);
			final CarouselColumn crousel2 = new CarouselColumn(img2, title2, text2, actions2);

			final List<Action> actions3 = Arrays.asList(Act31, Act32, Act33);
			final CarouselColumn crousel3 = new CarouselColumn(img3, title3, text3, actions3);

			final List<Action> actions4 = Arrays.asList(Act41, Act42, Act43);
			final CarouselColumn crousel4 = new CarouselColumn(img4, title4, text4, actions4);

			final List<Action> actions5 = Arrays.asList(Act51, Act52, Act53);
			final CarouselColumn crousel5 = new CarouselColumn(img5, title5, text5, actions5);

			final List<CarouselColumn> columns = Arrays.asList(crousel1, crousel2, crousel3, crousel4, crousel5);

//			final CarouselTemplate carouseltemplate = new CarouselTemplate(columns);

			/* CarouselTemplateはビルダークラスになっており、以下のように記述可能 */
			final CarouselTemplate carouseltemplate = CarouselTemplate.builder().imageAspectRatio(null).imageSize(null).columns(columns).build();

		return carouseltemplate;
	}

	/**
	 * こへっちカルーセルを生成
	 *
	 * @return カルーセルテンプレート
	 */
	public static CarouselTemplate kohecchiCreate(){

			//final String img1 = "https://oliva-production.s3-ap-northeast-1.amazonaws.com/uploads/item_image/image/3174/middle_d79b5c5a-47f1-48ea-b027-0326d7078da3.jpg";
			final String img1 = "https://d2dcan0armyq93.cloudfront.net/photo/odai/600/3b18ba340196b0a1ba064e9b6aa2cc71_600.jpg";
			final String title1 = "トレーニングモード";
			final String text1 = "トレーニングを選択してくださいｗｗｗ";
			final Action Act11 = new PostbackAction(CarouselButtonDispName.FUKKINN.getDispName(), CarouselButtonVal.TRAINING.getButtonValue() + 1);
			final Action Act12 = new PostbackAction(CarouselButtonDispName.HAIKIN.getDispName(), CarouselButtonVal.TRAINING.getButtonValue() + 2);
			final Action Act13 = new PostbackAction(CarouselButtonDispName.DAISYARIN.getDispName(), CarouselButtonVal.TRAINING.getButtonValue() + 3);

			//final String img2 = "https://cdn-ak.f.st-hatena.com/images/fotolife/M/Meshi2_IB/20171026/20171026153401.jpg";
			final String img2 = "https://i2.wp.com/gogon.net/sozaiya_gogon/wp-content/uploads/2010/06/i12-72a-01.png";
			final String title2 = "おでかけモード";
			final String text2 = "どこにおでかけしますか？";
			final Action Act21 = new PostbackAction(CarouselButtonDispName.AREA.getDispName(), CarouselButtonVal.ODEKAKE.getButtonValue() + 1);
			final Action Act22 = new PostbackAction(CarouselButtonDispName.CATEGORY.getDispName(), CarouselButtonVal.ODEKAKE.getButtonValue() + 2);
			final Action Act23 = new PostbackAction(CarouselButtonDispName.HIKIKOMORI.getDispName(), CarouselButtonVal.ODEKAKE.getButtonValue() + 3);

			//final String img3 = "http://tokyowise.jp/wp-content/uploads/2016/02/s2_160205_244.jpg";
			final String img3 = "https://iwiz-blog-cms.c.yimg.jp/c/blog-cms/series/feature/sc_top.jpg";
			final String title3 = "アンバサダーモード";
			final String text3 = "今日はどんな気分だい？";
			final Action Act31 = new PostbackAction(CarouselButtonDispName.HIGH.getDispName(), CarouselButtonVal.AMBASSADOR.getButtonValue() + 1);
			final Action Act32 = new PostbackAction(CarouselButtonDispName.NORMAL.getDispName(), CarouselButtonVal.AMBASSADOR.getButtonValue() + 2);
			final Action Act33 = new PostbackAction(CarouselButtonDispName.LOW.getDispName(), CarouselButtonVal.AMBASSADOR.getButtonValue() + 3);


			final List<Action> actions1 = Arrays.asList(Act11, Act12, Act13);
			final CarouselColumn crousel1 = new CarouselColumn(img1, title1, text1, actions1);

			final List<Action> actions2 = Arrays.asList(Act21, Act22, Act23);
			final CarouselColumn crousel2 = new CarouselColumn(img2, title2, text2, actions2);

			final List<Action> actions3 = Arrays.asList(Act31, Act32, Act33);
			final CarouselColumn crousel3 = new CarouselColumn(img3, title3, text3, actions3);

			final List<CarouselColumn> columns = Arrays.asList(crousel1, crousel2, crousel3);

//			final CarouselTemplate carouseltemplate = new CarouselTemplate(columns);

			/* CarouselTemplateはビルダークラスになっており、以下のように記述可能 */
			final CarouselTemplate carouseltemplate = CarouselTemplate.builder().imageAspectRatio(null).imageSize(null).columns(columns).build();

		return carouseltemplate;
	}
}