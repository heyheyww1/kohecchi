package builder;

import com.linecorp.bot.model.action.Action;
import com.linecorp.bot.model.action.DatetimePickerAction;
import com.linecorp.bot.model.message.template.ButtonsTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * 日付ボタンを生成するビルダークラス
 *
 */
public class DateButtonBuilder {

	/**
	 * 日付ボタンを生成
	 * 
	 * @param title 日付ボタンタイトル
	 * @param data 入力値を受け取るための値
	 * @return ボタンテンプレート
	 */
	public static ButtonsTemplate create(final String title,final String data){

		//カルーセルカラム用データ
		final String thumbnailImageUrl = "https://cdn.zuuonline.com/600/400/uploads/b20ff9cc1be050c1412e7ec5de0548c1.jpg";
		final String text = "「日付の選択」をクリックすると、ダイアログが開きます。";
		final String label = "日付の選択";
		final String mode = "date";
		final Action Act1 = new DatetimePickerAction(label, data, mode);
		final List<Action> actions = Arrays.asList(Act1);

//		final ButtonsTemplate buttonstemplate = new ButtonsTemplate(thumbnailImageUrl, title, text, actions);
		
		/* ButtonsTemplateはビルダークラスになっており、以下のように記述可能 */
		final ButtonsTemplate buttonstemplate = ButtonsTemplate.builder().title(title).text(text).actions(actions).build();

		return buttonstemplate;
	}
}
