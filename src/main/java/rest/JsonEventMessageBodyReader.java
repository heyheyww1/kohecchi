package rest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;
import com.linecorp.bot.client.LineSignatureValidator;
import com.linecorp.bot.model.event.CallbackRequest;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.servlet.LineBotCallbackRequestParser;

import common.constants.CredentialsConst;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;

/**
 * {@code List<Event>}用メッセージボディリーダー
 * 
 * @author oracle
 *
 */
@Provider
@Consumes(MediaType.APPLICATION_JSON)
public class JsonEventMessageBodyReader implements MessageBodyReader<List<Event>> {

	private static final Logger LOGGER = LoggerFactory.getLogger((new Object(){}.getClass().getEnclosingClass()));


	@Override
	public boolean isReadable(final Class<?> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType) {
		return type == List.class;
	}

	@Override
	public List<Event> readFrom(final Class<List<Event>> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType,
			final MultivaluedMap<String, String> httpHeaders, final InputStream entityStream)
			throws IOException, WebApplicationException {
		
		
		try {
			
			final LineSignatureValidator validator = new LineSignatureValidator(
					CredentialsConst.CHANNEL_SECRET.getBytes(StandardCharsets.UTF_8));
			final LineBotCallbackRequestParser parser = new LineBotCallbackRequestParser(validator);

			final String signature = httpHeaders.getFirst("X-Line-Signature");

			final byte[] jsonData = ByteStreams.toByteArray(entityStream);

			final String jsonText = new String(jsonData, StandardCharsets.UTF_8);

			// ログレベルがdebugならJSONレスポンスボディを出力する
			if (LOGGER.isDebugEnabled()) {

				final JSONObject jsonObj = new JSONObject(jsonText);

				LOGGER.debug("RESPONSE JSON\n" + jsonObj.toString(4));
			}

			CallbackRequest callbackRequest;

			callbackRequest = parser.handle(signature, jsonText);

			final List<Event> eventList = callbackRequest.getEvents();
			return eventList;
			
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
