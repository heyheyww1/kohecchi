package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JAX-RS設定クラス
 * 
 * @author oracle
 *
 */
@ApplicationPath("webresources")
public class ApplicationConfig extends Application {
    
}
