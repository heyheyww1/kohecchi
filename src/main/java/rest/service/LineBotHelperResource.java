package rest.service;

import com.linecorp.bot.client.LineMessagingServiceBuilder;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.BeaconEvent;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.JoinEvent;
import com.linecorp.bot.model.event.LeaveEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.ReplyEvent;
import com.linecorp.bot.model.event.UnfollowEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.linecorp.bot.model.response.BotApiResponse;

import action.BotAction;
import common.constants.CredentialsConst;
import common.dto.NewContractDTO;
import common.info.AppScopeUserInfo;
import common.info.ReqScopeUserBotInfo;

import java.io.IOException;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * LINEサーバからのリクエストを処理するエンドポイント用クラス
 * <p>
 * LINEサーバ -> Eventリクエストを受け取り、200 OKのレスポンスのみ返す<br>
 * LINE APIサーバ -> ユーザ端末に表示する内容をレスポンスとして返す
 * 
 * @author oracle
 *
 */
@RequestScoped
@Path("/callback")
public class LineBotHelperResource extends LineBotHelperBase {

	@Inject
	private AppScopeUserInfo appScopeUserInfo;

	@Inject
	ReqScopeUserBotInfo reqScopeUserBotInfo;

	@Inject
	private BotAction botaction;

	/**
	 * LINEサーバからEventリクエストを受け取り、200 OKのレスポンスを返却する
	 * 
	 * @param eventList LINEイベント一覧
	 * @return HTTPレスポンス
	 * @throws IOException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response receiveLINEServerRequest(final List<Event> eventList)
			throws IOException {

		LOGGER.info("callback from LINE received.");

		for (int i = 0; i < eventList.size(); i++) {

			final Event event = eventList.get(i);

			LOGGER.info("event[" + i + "]=" + event);

			if (event == null) {
				continue;
			}

			if (event instanceof MessageEvent) {
				final MessageEvent<?> messageEvent = (MessageEvent<?>) event;
				reply(handleMessageEvent(messageEvent));
				
			} else if (event instanceof UnfollowEvent) {
				final UnfollowEvent unfollowEvent = (UnfollowEvent) event;
				handleUnfollowEvent(unfollowEvent);
				
			} else if (event instanceof FollowEvent) {
				final FollowEvent followEvent = (FollowEvent) event;
				reply(handleFollowEvent(followEvent));
				
			} else if (event instanceof JoinEvent) {
				final JoinEvent joinEvent = (JoinEvent) event;
				reply(handleJoinEvent(joinEvent));
				
			} else if (event instanceof LeaveEvent) {
				final LeaveEvent leaveEvent = (LeaveEvent) event;
				handleLeaveEvent(leaveEvent);
				
			} else if (event instanceof PostbackEvent) {
				final PostbackEvent postbackEvent = (PostbackEvent) event;
				reply(handlePostbackEvent(postbackEvent));
				
			} else if (event instanceof BeaconEvent) {
				final BeaconEvent beaconEvent = (BeaconEvent) event;
				reply(handleBeaconEvent(beaconEvent));
			}

		}
		// LINEサーバには200 OKのみを返す
		return Response.status(Response.Status.OK).build();
	}

	/**
	 * TextMessageを受け取ったときの処理
	 */
	@Override
	public ReplyMessage handleTextMessageEvent(final MessageEvent<TextMessageContent> event) throws IOException {

		// ユーザーメッセージを取得
		final TextMessageContent userMessage = event.getMessage();
		final String userId = event.getSource().getUserId().toString();
		// ユーザーのProfileを取得する
		final UserProfileResponse userProfile = getUserProfile(event.getSource().getUserId());
		// LINE上での表示名
		LOGGER.info("=======DisplayName========" + userProfile.getDisplayName());

		// ユーザ情報を初期化
		appScopeUserInfo.initialize(userId);
		// ユーザーメッセージをエンティティに設定
		reqScopeUserBotInfo.setUserMessage(userMessage.getText());

		// アクションBeanを呼び出す
		botaction.execute(userId);

		return returnReplyMessage(event, userId);
	}

	/**
	 * PostbackEventを受け取ったときの処理
	 */
	@Override
	public ReplyMessage handlePostbackEvent(final PostbackEvent event) {
		LOGGER.info("do handle PostbackEvent");

		// ユーザーメッセージを取得
		final PostbackContent postbackContent = event.getPostbackContent();
		final String userId = event.getSource().getUserId().toString();
		final String data = postbackContent.getData();

		// ユーザ情報を初期化
		appScopeUserInfo.initialize(userId);
		// ユーザーメッセージをエンティティに設定
		reqScopeUserBotInfo.setUserMessage(data);
		
		final NewContractDTO newContractDTO = appScopeUserInfo.getNewContractMap().get(userId);
		// paramsがある場合はdateが入っている、paramsがない場合は違うデータが入っている
		if (postbackContent.getParams() != null && newContractDTO != null) {
			if(data.equals("startdate")) {
				final String startdate = postbackContent.getParams().get("date");
				reqScopeUserBotInfo.setUserMessage(startdate);
				newContractDTO.setStartdate(startdate);
				
			} else if (data.equals("enddate")) {
				final String enddate = postbackContent.getParams().get("date");
				reqScopeUserBotInfo.setUserMessage(enddate);
				newContractDTO.setEnddate(enddate);
				
			} else {
				newContractDTO.setData(data);
			}
		}
		// アクションBeanを呼び出す
		botaction.execute(userId);

		return returnReplyMessage(event, userId);
	}
	
	/**
	 * overrideしていないメッセージを受信した場合のデフォルトの処理
	 */
	@Override
	public ReplyMessage handleDefaultMessageEvent(final Event event) {
		// overrideしていないメッセージを受信した場合は何もしない(nullを返す)
		return null;
	}

	/**
	 * LINE APIサーバに返却するためのReplyMessageを生成する
	 * 
	 * @param event LINEイベント
	 * @param userId LINEユーザID
	 * @return LINE APIサーバへのReplyMessage
	 */
	private ReplyMessage returnReplyMessage(final ReplyEvent event, final String userId) {
		
		/* リプライするメッセージを設定 */
		final List<Message> messageList = reqScopeUserBotInfo.getBotMessageList();
		
		if(messageList.size() == 0){
			final String botResponseText = "・・・すみません、理解できませんでした。";
			messageList.add(new TextMessage(botResponseText));
		}
		
		return new ReplyMessage(event.getReplyToken(), messageList);
	}


	/**
	 * LINE APIサーバにはユーザ端末に表示する内容をレスポンスとして返す
	 * 
	 * @param replyMessage LINE APIサーバへのReplyMessage
	 * @throws IOException
	 */
	private void reply(final ReplyMessage replyMessage) throws IOException {
		LOGGER.info("send reply replyMessage=" + replyMessage);
		if (replyMessage == null) {
			return;
		}

		// LINE APIサーバにはユーザ端末に表示する内容をレスポンスとして返す
		final retrofit2.Response<BotApiResponse> response = LineMessagingServiceBuilder
				.create(CredentialsConst.CHANNEL_ACCESS_TOKEN).build().replyMessage(replyMessage)
				.execute();

		// show network level message
		LOGGER.info("send reply response=" + response.raw().toString());
	}

}