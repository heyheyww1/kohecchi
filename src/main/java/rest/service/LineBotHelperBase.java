package rest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.LineMessagingClientImpl;
import com.linecorp.bot.client.LineMessagingServiceBuilder;
import com.linecorp.bot.client.MessageContentResponse;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.BeaconEvent;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.JoinEvent;
import com.linecorp.bot.model.event.LeaveEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.UnfollowEvent;
import com.linecorp.bot.model.event.message.AudioMessageContent;
import com.linecorp.bot.model.event.message.ImageMessageContent;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.event.message.MessageContent;
import com.linecorp.bot.model.event.message.StickerMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.message.VideoMessageContent;
import com.linecorp.bot.model.profile.UserProfileResponse;

import common.constants.CredentialsConst;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

/**
 * LINEサーバからの飛んでくるEvent種別毎の処理を記述するためのベースクラス
 * 
 * @author oracle
 *
 */
public abstract class LineBotHelperBase {
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(new Object(){}.getClass().getEnclosingClass());
	
	@SuppressWarnings("unchecked")
	protected ReplyMessage handleMessageEvent(final MessageEvent<?> messageEvent) throws IOException {

		final MessageContent messageContent = messageEvent.getMessage();
		if (messageContent == null) {
			return null;
		}

		LOGGER.info("message content=" + messageContent);

		if (messageContent instanceof TextMessageContent) {
			final MessageEvent<TextMessageContent> event = (MessageEvent<TextMessageContent>) messageEvent;
			return handleTextMessageEvent(event);
			
		} else if (messageContent instanceof ImageMessageContent) {
			final MessageEvent<ImageMessageContent> event = (MessageEvent<ImageMessageContent>) messageEvent;
			return handleImageMessageEvent(event);
			
		} else if (messageContent instanceof LocationMessageContent) {
			final MessageEvent<LocationMessageContent> event = (MessageEvent<LocationMessageContent>) messageEvent;
			return handleLocationMessageEvent(event);
			
		} else if (messageContent instanceof AudioMessageContent) {
			final MessageEvent<AudioMessageContent> event = (MessageEvent<AudioMessageContent>) messageEvent;
			return handleAudioMessageEvent(event);
			
		} else if (messageContent instanceof VideoMessageContent) {
			final MessageEvent<VideoMessageContent> event = (MessageEvent<VideoMessageContent>) messageEvent;
			return handleVideoMessageEvent(event);
			
		} else if (messageContent instanceof StickerMessageContent) {
			final MessageEvent<StickerMessageContent> event = (MessageEvent<StickerMessageContent>) messageEvent;
			return handleStickerMessageEvent(event);
			
		} else {
			return null;
		}
	}
	
	/**
	 * When other messages not overridden as handle* is received.
	 * 
	 * @param event
	 * @return
	 */
	protected abstract ReplyMessage handleDefaultMessageEvent(Event event);
	
	/**
	 * Called when a TextMessage is received
	 * 
	 * @param event
	 * @return
	 * @throws IOException
	 */
	protected ReplyMessage handleTextMessageEvent(final MessageEvent<TextMessageContent> event)
			throws IOException {
		LOGGER.info("do handle TextMessageEvent");
		return handleDefaultMessageEvent(event);
	}
	
	/**
	 * Called when a PostbackEvent is received
	 *
	 * @param event
	 * @return
	 */
	protected ReplyMessage handlePostbackEvent(final PostbackEvent event) {
		LOGGER.info("do handle PostbackEvent");
		return handleDefaultMessageEvent(event);
	}
	
	/**
	 * Called when a ImageMessage is received
	 *
	 * @param event
	 * @return
	 * @throws IOException
	 */
	protected ReplyMessage handleImageMessageEvent(final MessageEvent<ImageMessageContent> event)
			throws IOException {
		LOGGER.info("do handle ImageMessageEvent");
		return handleDefaultMessageEvent(event);
	}

	/**
	 * Called when a LocationMessage is received
	 *
	 * @param event
	 * @return
	 * @throws IOException
	 */
	protected ReplyMessage handleLocationMessageEvent(
			final MessageEvent<LocationMessageContent> event) {
		LOGGER.info("do handle LocationMessageEvent");
		return handleDefaultMessageEvent(event);
	}

	/**
	 * Called when a StickerMessage is received
	 *
	 * @param event
	 * @return
	 */
	protected ReplyMessage handleStickerMessageEvent(
			final MessageEvent<StickerMessageContent> event) {
		LOGGER.info("do handle StickerMessageEvent");
		return handleDefaultMessageEvent(event);
	}

	/**
	 * Called when a AudioMessage is received
	 *
	 * @param event
	 * @return
	 * @throws IOException
	 */
	protected ReplyMessage handleAudioMessageEvent(final MessageEvent<AudioMessageContent> event)
			throws IOException {
		LOGGER.info("do handle AudioMessageEvent");
		return handleDefaultMessageEvent(event);
	}

	/**
	 * Called when a VideoMessage is received
	 *
	 * @param event
	 * @return
	 * @throws IOException
	 */
	protected ReplyMessage handleVideoMessageEvent(final MessageEvent<VideoMessageContent> event)
			throws IOException {
		LOGGER.info("do handle VideoMessageEvent");
		return handleDefaultMessageEvent(event);
	}

	/**
	 * Called when a UnfollowEvent is received
	 *
	 * @param event
	 */
	protected void handleUnfollowEvent(final UnfollowEvent event) {
		LOGGER.info("do handle UnfollowEvent");
		handleDefaultMessageEvent(event);
	}

	/**
	 * Called when a FollowEvent is received
	 *
	 * @param event
	 * @return
	 */
	protected ReplyMessage handleFollowEvent(final FollowEvent event) {
		LOGGER.info("do handle FollowEvent");
		return handleDefaultMessageEvent(event);
	}

	/**
	 * Called when a JoinEvent is received
	 *
	 * @param event
	 * @return
	 */
	protected ReplyMessage handleJoinEvent(final JoinEvent event) {
		LOGGER.info("do handle JoinEvent");
		return handleDefaultMessageEvent(event);
	}

	/**
	 * Called when a LeaveEvent is received
	 *
	 * @param event
	 */
	protected void handleLeaveEvent(final LeaveEvent event) {
		LOGGER.info("do handle LeaveEvent");
		handleDefaultMessageEvent(event);
	}

	/**
	 * Called when a BeaconEvent is received
	 *
	 * @param event
	 * @return
	 */
	protected ReplyMessage handleBeaconEvent(final BeaconEvent event) {
		LOGGER.info("do handle BeaconEvnt");
		return handleDefaultMessageEvent(event);

	}

	/**
	 * Get User Profile
	 *
	 * @param userId
	 * @return
	 */
	public UserProfileResponse getUserProfile(final String userId) {

		final LineMessagingClient lineMessagingClient = new LineMessagingClientImpl(
				LineMessagingServiceBuilder.create(CredentialsConst.CHANNEL_ACCESS_TOKEN).build());
		try {
			final UserProfileResponse userProfileResponse = lineMessagingClient.getProfile(userId)
					.get();
			return userProfileResponse;

		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Get InputStream of contents such as images
	 *
	 * @param content
	 * @return
	 */
	public InputStream getContentStream(final MessageContent content) {

		final String messageId = content.getId();

		final LineMessagingClient lineMessagingClient = new LineMessagingClientImpl(
				LineMessagingServiceBuilder.create(CredentialsConst.CHANNEL_ACCESS_TOKEN).build());

		try {
			MessageContentResponse res;
			res = lineMessagingClient.getMessageContent(messageId).get();

			final InputStream contentStream = res.getStream();
			return contentStream;

		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}

}
