package rest.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 契約履歴情報REST用DTO
 * @author oracle
 *
 */
@XmlRootElement
public class RestKeiyakuRireki {

    @Getter @Setter
    private Long keiyakuId; // 契約ID
    @Getter @Setter
    private String userId; // 顧客ID
    @Getter @Setter
    private String kokyakuname; // 氏名
    @Getter @Setter
    private Long hokenId; // 保険ID
    @Getter @Setter
    private String hokenmei; // 保険名
    @Getter @Setter
    private String kaishibi; // 開始日
    @Getter @Setter
    private String syuryobi; // 終了日

    public RestKeiyakuRireki() {
    }

    public RestKeiyakuRireki(final Long keiyakuId, final String userId, final String kokyakuname, final Long hokenId, final String hokenmei, final String kaishibi, final String syuryobi) {
        this.keiyakuId = keiyakuId;
        this.userId = userId;
        this.kokyakuname = kokyakuname;
        this.hokenId = hokenId;
        this.hokenmei = hokenmei;
        this.kaishibi = kaishibi;
        this.syuryobi = syuryobi;
    }
}
