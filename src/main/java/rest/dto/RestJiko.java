package rest.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 事故情報REST用DTO
 * @author oracle
 *
 */
@XmlRootElement
public class RestJiko {
	
    @Getter @Setter
    private Long jikoId; // 事故ID
	@Getter @Setter
    private Long keiyakuId; // 契約ID	
	@Getter @Setter
    private String kagaisya; // 加害者	
	@Getter @Setter
    private String higaisya; // 被害者	
	@Getter @Setter
    private String hasseibi; // 発生日	
	@Getter @Setter
    private String hasseibasyo; // 発生場所	

    public RestJiko() {
    }

}
