package rest.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 給付情報REST用DTO
 * @author oracle
 *
 */
@XmlRootElement
public class RestKyuhu {

    @Getter @Setter
    private Long kyuhuId; // 給付ID
    @Getter @Setter
    private Long keiyakuId; // 契約ID
    @Getter @Setter
    private String userId; // 顧客ID
    @Getter @Setter
    private String getsugaku; // 月額
    @Getter @Setter
    private String kaishibi; // 開始日
    @Getter @Setter
    private String syuryobi; // 終了日

    public RestKyuhu() {
    }
}
