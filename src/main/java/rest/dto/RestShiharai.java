package rest.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 支払情報REST用DTO
 * @author oracle
 *
 */
@XmlRootElement
public class RestShiharai {

    @Getter @Setter
    private Long shiharaiId; // 支払ID
    @Getter @Setter
    private String userId; // 顧客ID
    @Getter @Setter
    private Long seikyuId; // 請求ID
    @Getter @Setter
    private String getsugaku; // 月額
    @Getter @Setter
    private String kaishibi; // 開始日
    @Getter @Setter
    private String syuryobi; // 終了日

    public RestShiharai() {
    }
}