package rest.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 顧客情報REST用DTO
 * @author oracle
 *
 */
@XmlRootElement
public class RestKokyaku {

	@Getter @Setter
    private String userId; // 顧客ID
    @Getter @Setter
    private String kokyakuname; // 氏名
    @Getter @Setter
    private String kokyakunamekana; // 氏名(カナ)
    @Getter @Setter
    private String yubinbango; // 郵便番号
    @Getter @Setter
    private String jusyo; // 住所
    @Getter @Setter
    private String syokugyo; // 職業
    @Getter @Setter
    private String seibetsu; // 性別

    public RestKokyaku(final String userId, final String kokyakuname, final String kokyakunamekana, final String yubinbango, final String jusyo, final String syokugyo, final String seibetsu) {
        this.userId = userId;
        this.kokyakuname = kokyakuname;
        this.kokyakunamekana = kokyakunamekana;
        this.jusyo = jusyo;
        this.yubinbango = yubinbango;
        this.syokugyo = syokugyo;
        this.seibetsu = seibetsu;
    }

}