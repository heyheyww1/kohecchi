package service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rest.dto.RestKeiyaku;

import javax.enterprise.context.Dependent;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * mockのREST APIを実行するRESTクライアントクラス
 * 
 * @author oracle
 *
 */
@Dependent
public class EntityActionService {

	WebTarget webTarget = ClientBuilder.newClient().target("http://tmnweblogic-wls-1:9073/mock/webresources");
    
    private static final Logger LOGGER = LoggerFactory.getLogger(new Object(){}.getClass().getEnclosingClass());

    /**
     * 契約情報を登録する
     * <p>
     * ★[新規契約]
     * <p>
     * @param restKeiyaku 契約新規作成内容
     * @return 契約新規作成結果
     */
    public RestKeiyaku createKeiyaku(final RestKeiyaku restKeiyaku) {
    	final WebTarget webTarget = this.webTarget.path("keiyaku");
    	final Response response = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.entity(restKeiyaku, "application/json;charset=UTF-8"), Response.class);
    	RestKeiyaku result = null;
    	if(response.getStatus() == Response.Status.CREATED.getStatusCode()){
    		result = response.readEntity(RestKeiyaku.class);
    	} else {
    		LOGGER.warn("契約情報の登録に失敗===="+response.getStatus());
    	}
    	response.close();
		return result;
    }
}