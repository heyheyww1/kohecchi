package common.constants;

/**
 * カルーセルに出力するボタンの表示名ENUM
 *
 */
public enum CarouselButtonDispName {

	KEIYAKU("契約"), KAIYAKU("解約"), KEIYAKU_HENKO("契約変更"),
	FUKKINN("腹筋"),HAIKIN("背筋"),DAISYARIN("大車輪"),
	AREA("エリアから選ぶ"),CATEGORY("ジャンルから探す"),HIKIKOMORI("おでかけしない"),
	SEA("海"), MOUNTAIN("山"), RIVER("川"),
	HIGH("最高だよ"),NORMAL("まあまあだじょ"),LOW("最低の気分だよ");

    private final String dispName;

    private CarouselButtonDispName(final String dispName) {
        this.dispName = dispName;
    }

    public String getDispName() {
        return this.dispName;
    }
}
