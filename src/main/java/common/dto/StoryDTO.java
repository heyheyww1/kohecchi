package common.dto;

import lombok.Data;

/**
 * ユーザの現在のストーリーを管理するDTO
 * 
 * @author oracle
 *
 */
@Data
public class StoryDTO {
	
    private String story;
    private String storyCategory;
}
