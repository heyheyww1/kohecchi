package common.dto;

import lombok.Data;

/**
 * ユーザの現在の筋トレ回数を管理するDTO
 *
 * @author oracle
 *
 */
@Data
public class TimesDTO {

    private String times;
    private String timesCategory;
}
