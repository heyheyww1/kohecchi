package common.dto;

import lombok.Data;

/**
 * 新規契約機能用DTO
 * 
 * @author oracle
 *
 */
@Data
public class NewOdekakeDTO {
	
    private String worktype;
    private String kokyakuname;
    private String startdate;
    private String enddate;
    private String data;
}
