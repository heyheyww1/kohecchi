package common.info;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import common.dto.NewContractDTO;
import common.dto.NewOdekakeDTO;
import common.dto.StoryDTO;
import common.dto.TimesDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * LINEユーザ毎の情報保持用クラス
 *
 */
@ApplicationScoped
public class AppScopeUserInfo {

	@Getter
	@Setter
	private Map<String, StoryDTO> storyMap;

	@Getter
	@Setter
	private Map<String, TimesDTO> timesMap;

	@Getter
	@Setter
	private Map<String, NewContractDTO> newContractMap;

	@Getter
	@Setter
	private Map<String, NewOdekakeDTO> newOdekakeMap;


	// 改行コードサンプル
	// String sep = System.getProperty("line.separator");

	/**
	 * 初期化処理
	 */
	@PostConstruct
	public void PostConstruct() {
		storyMap = new HashMap<>();
		timesMap = new HashMap<>();
		newContractMap = new HashMap<>();
	}

	/**
	 * LINEをユーザからのメッセージを設定
	 *
	 * @param userId LINEユーザID
	 * @param userMessage LINEユーザからのメッセージ
	 */
	public void initialize(final String userId) {

		// ユーザー情報が存在しなければ生成
		if (!storyMap.containsKey(userId)) {
			final StoryDTO storyDTO = new StoryDTO();
			storyDTO.setStory("none");
			storyDTO.setStoryCategory("none");
			storyMap.put(userId, storyDTO);
		}

		// ユーザー情報が存在しなければ生成
		if (!timesMap.containsKey(userId)) {
			final TimesDTO timesDTO = new TimesDTO();
			timesDTO.setTimes("none");
			timesDTO.setTimesCategory("none");
			timesMap.put(userId, timesDTO);
		}
	}
}
