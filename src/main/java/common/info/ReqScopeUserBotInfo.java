package common.info;

import com.linecorp.bot.model.message.Message;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

/**
 * ユーザから受け取ったメッセージ・BOTが返すメッセージの保持用クラス
 *
 * @author oracle
 *
 */
@RequestScoped
public class ReqScopeUserBotInfo {
	
	@Getter
	@Setter
    private String userMessage;
	
	@Getter
	@Setter
	private List<Message> botMessageList;
	
	@PostConstruct
	public void initialize() {
		botMessageList = new ArrayList<>();
	}
}
